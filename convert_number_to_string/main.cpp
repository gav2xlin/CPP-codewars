#include <iostream>
#include <string>
#include <cmath>
#include <cassert>

std::string number_to_string(int num) {
    std::string res;

    bool flag = num < 0;
    num = abs(num);

    while (num > 0) {
        res = static_cast<char>(num % 10 + '0') + res;
        num /= 10;
    }

    if (flag) res = '-' + res;

    return res;
}

int main()
{
    assert(number_to_string(1+2) == "3");
    assert(number_to_string(67) == "67");
    assert(number_to_string(-5) == "-5");

    return 0;
}
