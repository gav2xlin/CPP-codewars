#include <iostream>
#include <vector>

using namespace std;

std::vector<int> get_even_numbers(const std::vector<int>& arr) {
    std::vector<int> res;
    for (auto d : arr) {
        if (d % 2 == 0) {
            res.push_back(d);
        }
    }

    return res;
}

void assert_is_equal(const std::vector<int>& x, const std::vector<int>& y) {
    if (get_even_numbers(x) != y) {
        abort();
    }
}

int main() {
    assert_is_equal({2, 4, 5, 6}, {2, 4, 6});
    assert_is_equal({}, {});
    assert_is_equal({1}, {});
    assert_is_equal({1, 2}, {2});
    assert_is_equal({1, 2, 3, 4, 5}, {2, 4});
    assert_is_equal({2, 4, 6, 8}, {2, 4, 6, 8});

    return 0;
}
