#include <iostream>
#include <vector>
#include <sstream>
#include <stdexcept>

using namespace std;

void merge_partitions(std::vector<int>& nums, int l, int m, int r) {
    int ls = m - l + 1, rs = r - m;

    std::vector<int> left(ls);
    std::vector<int> right(rs);

    for (int i = 0; i < ls; ++i) {
        left[i] = nums[l + i];
    }

    for (int j = 0; j < rs; ++j) {
        right[j] = nums[m + j + 1];
    }

    int i = 0, j = 0, k = l;
    while (i < ls && j < rs) {
        if (left[i] < right[j]) {
            nums[k++] = left[i++];
        } else {
            nums[k++] = right[j++];
        }
    }

    while (i < ls) {
        nums[k++] = left[i++];
    }

    while (j < rs) {
        nums[k++] = right[j++];
    }
}

void merge_sort(std::vector<int>& nums, int l, int r) {
    if (l < r) {
        int m = l + (r - l) / 2;
        merge_sort(nums, l, m);
        merge_sort(nums, m + 1, r);

        merge_partitions(nums, l, m, r);
    }
}

std::vector<int> solution(std::vector<int> nums) {
    merge_sort(nums, 0, nums.size() - 1);
    return nums;
}

std::ostream& operator<<(std::ostream& os, const std::vector<int>& vec) {
    bool flag{false};
    for (auto d : vec) {
        if (flag) os << ' ';
        os << d;
        flag = true;
    }
    return os;
}

#define VECTOR(...) std::vector<int>{__VA_ARGS__}

#define IS_EQUAL(x, y) if (solution(x) != y) { \
    stringstream ss; \
    ss << x << " != " << y; \
    throw logic_error(ss.str()); \
}

int main()
{
    IS_EQUAL(VECTOR(1, 2, 3, 10, 5), VECTOR(1, 2, 3, 5, 10))
    IS_EQUAL(VECTOR(), VECTOR())
    IS_EQUAL(VECTOR(20, 2, 10), VECTOR(2, 10, 20))
    IS_EQUAL(VECTOR(2, 20, 10), VECTOR(2, 10, 20));

    return 0;
}
