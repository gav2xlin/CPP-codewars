#include <iostream>
#include <cassert>

using namespace std;

int rental_car_cost(int d){
    return d * 40 - (d >= 7 ? 50 : (d >= 3 ? 20 : 0));
}

int main()
{
    assert(rental_car_cost(1) == 40);
    assert(rental_car_cost(2) == 80);
    assert(rental_car_cost(3) == 100);
    assert(rental_car_cost(4) == 140);
    assert(rental_car_cost(5) == 180);
    assert(rental_car_cost(6) == 220);
    assert(rental_car_cost(7) == 230);
    assert(rental_car_cost(8) == 270);
    assert(rental_car_cost(9) == 310);
    assert(rental_car_cost(10) == 350);

    return 0;
}
