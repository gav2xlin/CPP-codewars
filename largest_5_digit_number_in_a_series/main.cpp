#include <iostream>
#include <string>

int largest_five_digits(const std::string &digits)
{
    int max = 0, cur = 0;
    for (std::size_t i = 0; i < digits.length(); ++i) {
      int d = digits[i] - '0';
      cur = cur * 10 + d;
      if (cur > max) max = cur;
      if (i >= 4) cur %= 10000;
    }
    return max;
}

int main()
{
    std::cout << largest_five_digits("1234567890") << std::endl; // 67890
    return 0;
}
