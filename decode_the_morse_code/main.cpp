#include <iostream>
#include <cassert>
#include <unordered_map>
#include <vector>
#include <string>

using namespace std;

const vector<string> morse{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", ".-.-.-", "--..--", "..--..", ".----.", "-.-.--", "-..-.", "-.--.", "-.--.-", ".-...", "---...", "-.-.-.", "-...-", ".-.-.", "-....-", "..--.-", ".-..-.", "...-..-", ".--.-.", "...---..."};
const vector<string> ascii{"A",  "B",    "C",    "D",   "E", "F",    "G",   "H",    "I",  "J",    "K",   "L",    "M",  "N",  "O",   "P",    "Q",    "R",   "S",   "T", "U",   "V",    "W",   "X",    "Y",    "Z",    "0",     "1",     "2",     "3",     "4",     "5",     "6",     "7",     "8",     "9",     ".",      ",",      "?",      "'",      "!",      "/",     "(",     ")",      "&",     ":",      ";",      "=",     "+",     "-",      "_",      "\"",     "$",       "@",      "SOS"};

unordered_map<string, string> init_morse_map() {
    unordered_map<string, string> res;

    for (int i = 0; i < morse.size(); ++i) {
        res[morse[i]] = ascii[i];
    }

    return res;
}

const unordered_map<string, string> MORSE_CODE = init_morse_map();

std::string decodeMorse(std::string morseCode) {
    std::string decoded, seq;
    bool is_space{false};

    for(auto p : morseCode) {
        if (p == '.' || p == '-') {
            if (is_space && !decoded.empty()) decoded += ' ';
            seq += p;
            is_space = false;
        } else if (p == ' ') {
            if (!seq.empty()) {
                decoded += MORSE_CODE.at(seq);
                seq.clear();
            } else {
                is_space = true;
            }
        }
    }

    if (!seq.empty()) {
        decoded += MORSE_CODE.at(seq);
    }

    return decoded;
}

int main()
{
    assert(decodeMorse(".... . -.--   .--- ..- -.. .") == "HEY JUDE");

    return 0;
}
