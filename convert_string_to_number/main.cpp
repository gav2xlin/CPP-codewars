#include <iostream>
#include <string>
#include <cassert>

using namespace std;

int string_to_number(const std::string& s) {
    int res = 0, pos = 1;
    for (auto it = s.rbegin(); it != s.rend(); ++it) {
        if (*it == '-') {
            // this condition is simplified
            res *= -1;
        } else {
            res += (*it - '0') * pos;
            pos *= 10;
        }
    }
    return res;
}

int main()
{
    assert(string_to_number("123456") == 123456);
    assert(string_to_number("352605") == 352605);
    assert(string_to_number("-321405") == -321405);
    assert(string_to_number("-7") == -7);
    assert(string_to_number("0") == 0);
    assert(string_to_number("-0") == 0);

    return 0;
}
