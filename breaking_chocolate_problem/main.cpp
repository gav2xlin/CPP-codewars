#include <iostream>
#include <cassert>

using namespace std;

int break_chocolate(int n, int m) {
    return n > 0 && m > 0 ? n * m - 1 : 0;
}

int main()
{
    assert(break_chocolate(5, 5) == 24);
    assert(break_chocolate(7, 4) == 27);
    assert(break_chocolate(1, 1) == 0);
    assert(break_chocolate(0, 0) == 0);
    assert(break_chocolate(6, 1) == 5);

    return 0;
}
